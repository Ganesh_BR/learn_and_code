package little_monk;

class Stack {
	int stack[], top = -1, maxCount = 0, largestCount = 0, count = 0;

	public Stack(int size) {
		this.stack = new int[size];
	}

	public void push(int positiveNum) {
		stack[++top] = positiveNum;
	}

	int pop(int negativeNum) {

		if (top == -1) {
			return maxCount;
		} else if (stack[top] == (-1 * negativeNum)) {

			count += 2;
			top--;
			if (top == -1) {
				maxCount += count;
				count = 0;
			}
			return Math.max(maxCount, count);
		} else {
			top = -1;
			maxCount = Math.max(maxCount, count);
			return maxCount;
		}
	}
}
