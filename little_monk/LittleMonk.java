package little_monk;

import java.util.Scanner;

public class LittleMonk {
	static int count=0;
	public static void main(String[] args) {
		Scanner scan = new Scanner(System.in);
		int numberOfInputs = scan.nextInt();
		Stack s1 = new Stack(numberOfInputs);
		for (int i = 0; i < numberOfInputs; i++) {
			int var = scan.nextInt();
			if (var > 0) {
				s1.push(var);
			} else if (var < 0) {
				count = s1.pop(var);
			}
		}
		System.out.println(count);
	}
}

